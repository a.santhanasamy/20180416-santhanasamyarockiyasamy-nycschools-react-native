import React from 'react';
import { Image, View } from 'react-native';
import appStyle from '../../res/styles/app_style';
import { getIcon, Strings } from '../utils/Resources';

export default class SATScoreListScreen extends React.Component {

    static navigationOptions = {
        title : Strings.str_SAT_score
    }
    render() {
        return (

            <View style={appStyle.root_container}>
                <Image
                    source={require('../../res/icons/ic_score.png')}
                    style={[appStyle.icon]}
                />
            </View>
        );
    }
}