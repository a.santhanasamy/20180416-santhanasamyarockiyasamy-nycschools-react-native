import React, { Component }  from "react";
import { FlatList } from "react-native";
import { List, ListItem } from 'react-native-elements';
import { Strings } from '../utils/Resources';

export default class SchoolListScreen1 extends React.Component {

    // static navigationOptions = {
    //     title: Strings.str_NYC_School
    // }

    // constructor(props) {
    //     super(props);

    //     this.state = {
    //         //selected: new Map(),
    //         loading: false,
    //         data: [],
    //         page: 1,
    //         seed: 1,
    //         error: null,
    //         refreshing: false
    //     };
    // }

    // // _keyExtractor = (item, index) => item.id;

    // // _onPressItem = (id) => {
    // //     // updater functions are preferred for transactional updates
    // //     this.setState((state) => {
    // //         // copy the map rather than modifying state.
    // //         const selected = new Map(state.selected);
    // //         selected.set(id, !selected.get(id)); // toggle
    // //         return { selected };
    // //     });
    // // };

    // // _renderItem = ({ item }) => (
    // //     <SchoolListItem
    // //         id={item.id}
    // //         onPressItem={this._onPressItem}
    // //         selected={!!this.state.selected.get(item.id)}
    // //         title={item.title}
    // //     />
    // // );

    // // render1() {
    // //     return (
    // //         <FlatList
    // //             data={this.props.data}
    // //             extraData={this.state}
    // //             keyExtractor={this._keyExtractor}
    // //             renderItem={this._renderItem}
    // //         />
    // //     );
    // // }

    // componentDidMount() {
    //     this.makeRemoteRequest();
    // }

    // render() {
    //     return (
    //         <List containerStyle={{ borderTopWidth: 0, borderBottomWidth: 0 }}>
    //             <FlatList
    //                 data={this.state.data}
    //                 renderItem={({ item }) => (
    //                     <ListItem
    //                         title={'${item.name.first} ${item.name.last}'}
    //                         subtitle={item.email}
    //                         leftAvatar={{ rounded: true, source: { uri: item.picture.thumbnail } }}
    //                         // avatar={{ uri: item.picture.thumbnail }}
    //                         containerStyle={{ borderBottomWidth: 0 }}
    //                     />
    //                 )}
    //                 keyExtractor={item => item.email}
    //             />
    //         </List>
    //     );
    // }

    // // keyExtractor = () => {
    // //     item => item.email
    // // }
    // // renderItem = ({ item }) => {

    // //     return (
    // //         <ListItem>
    // //             roundAvatar
    // //             title='{item.name.first}{item.name.last}
    // //             subTitle={item.email}
    // //             containerStyle={borderWidth = 0}
    // //         </ListItem>
    // //     )
    // // }

    // makeRemoteRequest = () => {
    //     const { page, seed } = this.state;
    //     const url = `https://randomuser.me/api/?seed=${seed}&page=${page}&results=20`;
    //     this.setState({ loading: true });

    //     fetch(url)
    //         .then(res => res.json())
    //         .then(res => {
    //             this.setState({
    //                 data: page === 1 ? res.results : [...this.state.data, ...res.results],
    //                 error: res.error || null,
    //                 loading: false,
    //                 refreshing: false
    //             });
    //         })
    //         .catch(error => {
    //             this.setState({ error, loading: false });
    //         });
    // };

}