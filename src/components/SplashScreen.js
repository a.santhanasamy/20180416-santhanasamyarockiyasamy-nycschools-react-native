
import React from 'react';
import { View } from 'react-native';
import Logo from '../../res/icons/ic_app_logo_white.svg';
import appStyle, { appTheme } from '../../res/styles/app_style';
import { resetStackAction } from '../utils/NavigationUtils';

export default class SplashScreen extends React.Component {

    static navigationOptions = {
        header: null,
    }

    async componentDidMount() {

        const data = await this.performTimeConsumingTask();
        if (data !== null) {
            this.setState({ isLoading: false });
        }
    }
    performTimeConsumingTask = async () => {
        return new Promise((resolve) =>
            setTimeout(
                () => {
                    resetStackAction(this, 'home')
                },
                2000
            )
        );
    }

    render() {
        return (
            <View style={appStyle.root_container}>
                <Logo style={appTheme.splash_icon} />
            </View>
        );
    }
}