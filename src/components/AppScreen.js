import React, { Component } from "react";
import { Dimensions } from "react-native";

class AppScreen extends Component {

    constructor(props) {
        super(props)
        state = Dimensions.get("window");
        handler = dims => this.setState(dims)
    }

    componentWillMount() {
        Dimensions.addEventListener("change", this.handler);
    }

    componentWillUnmount() {
        // Important to stop updating state after unmount
        Dimensions.removeEventListener("change", this.handler);
    }

    render() {
        const { width, height } = this.state.window;
        const mode = height > width ? "portrait" : "landscape";
        console.debug('New dimensions ${width}x${height} (${mode})');
        return null;
    }
}
export default AppScreen