import React from 'react';
import { Image, View } from 'react-native';
import appStyle from '../../res/styles/app_style';

export default class Signupcreen extends React.Component {

    render() {
        return (

            <View style={appStyle.root_container}>
                <Image
                    source={require('../../res/icons/ic_score.png')}
                    style={[appStyle.icon]}
                />
            </View>
        );
    }
}
