import React from "react";
import { Image, Text, TouchableOpacity, View } from 'react-native';
import common, { appTheme, padding } from '../../res/styles/app_style';
import { getActionIcon, Strings } from '../utils/Resources';
import { ScrollView } from "react-native-gesture-handler";

export default class SchoolDetailScreen extends React.Component {

    static navigationOptions = {
        title: Strings.str_NYC_school_detail
    }

    constructor(props) {
        super(props);
        console.info(" Display info for the school ->  " + this.props.navigation.getParam('Key_Extra_SchoolInfo'));
        this.state = {
            schoolInfo: this.props.navigation.getParam('Key_Extra_SchoolInfo')
        }
    }

    render() {
        return (
            <ScrollView removeClippedSubviews={true} onPress={this.onSchoolClicked} style={[common.inner_container_with_border]}>
                <View style={common.row_container}>
                    <Text style={[appTheme.list_item_header_bold, { flex: 0.8 }]}>{
                        this.state.schoolInfo.school_name}
                    </Text>
                    <Text style={[appTheme.list_item_header, { flex: 0.2, textAlign: 'right' }]}>{this.state.schoolInfo.dbn}</Text>
                </View>

                <Text numberOfLines={10} ellipsizeMode={'tail'} style={[appTheme.list_item_content_text]}>{this.state.schoolInfo.overview_paragraph}</Text>

                <Text style={appTheme.list_item_content_header_text}>Contact</Text>

                <View style={[common.row_container, { marginTop: padding.sm }]}>
                    <Image style={appTheme.action_icon} source={getActionIcon('ic_action_phone')} />
                    <Text style={appTheme.list_item_content_text}>{this.state.schoolInfo.phone_number}</Text>
                </View>

                <View style={common.row_container}>
                    <Image style={appTheme.action_icon} source={getActionIcon('ic_action_fax')} />
                    <Text style={appTheme.list_item_content_text}>{this.state.schoolInfo.fax_number}</Text>
                </View>

                <View style={common.row_container}>
                    <Image style={appTheme.action_icon} source={getActionIcon('ic_action_email')} />
                    <Text style={appTheme.list_item_content_text}>{this.state.schoolInfo.school_email}</Text>
                </View>

                <View style={common.row_container}>
                    <Image style={appTheme.action_icon} source={getActionIcon('ic_action_web')} />
                    <Text style={appTheme.list_item_content_text}>{this.state.schoolInfo.website}</Text>
                </View>

                <View style={common.row_container}>
                    <Image style={appTheme.action_icon} source={getActionIcon('ic_action_location')} />
                    <Text style={appTheme.list_item_content_text}>{this.state.schoolInfo.location}</Text>
                </View>


                <Text style={appTheme.list_item_content_header_text}>Transport</Text>

                <View style={[common.row_container, { marginTop: padding.sm }]}>
                    <Image style={appTheme.action_icon} source={getActionIcon('ic_action_bus')} />
                    <Text style={appTheme.list_item_content_text}>{this.state.schoolInfo.bus}</Text>
                </View>

                <View style={[common.row_container, { marginTop: padding.sm }]}>
                    <Image style={appTheme.action_icon} source={getActionIcon('ic_action_subway')} />
                    <Text style={appTheme.list_item_content_text}>{this.state.schoolInfo.subway}</Text>
                </View>

                <Text style={appTheme.list_item_content_header_text}>Sports</Text>

                <View style={[common.row_container, { marginTop: padding.sm }]}>
                    <Image style={appTheme.action_icon} source={getActionIcon('ic_action_play')} />
                    <Text style={appTheme.list_item_content_text}>{this.state.schoolInfo.school_sports}</Text>
                </View>

                <Text style={appTheme.list_item_content_header_text}>Timing</Text>

                <View style={[common.row_container, { marginTop: padding.sm, marginBottom: padding.xl }]}>
                    <Image style={appTheme.action_icon} source={getActionIcon('ic_action_timing')} />
                    <Text style={appTheme.list_item_content_text}>Start Time : {this.state.schoolInfo.start_time}</Text>
                    <Text style={appTheme.list_item_content_text}>End Time   : {this.state.schoolInfo.end_time}</Text>
                </View>
            </ScrollView>
        );
    }
}