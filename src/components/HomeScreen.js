import React from "react";
import { Image, Text, TouchableOpacity, View } from 'react-native';
import { appTheme } from '../../res/styles/app_style';
import { moveToScreen } from "../utils/NavigationUtils";
import { getIcon, Strings } from '../utils/Resources';
import AppScreen from "./AppScreen";

export default class HomeScreen extends AppScreen {

    constructor(props) {
        super(props)
        this.state = {
            count: 0
        }
    }
    static navigationOptions = {
        title: "NYC Schools",
        // headerLeft: null,
        headerTitleStyle: appTheme.action_bar_center_title,
    }

    componentDidMount() {
        // resetStackAction(this, 'home')
    }

    render() {
        //super.render();
        return (

            <View style={appTheme.dash_board_container}>
                {this.renderRow(getIcon('ic_dash_school'), getIcon('ic_dash_scores'), Strings.str_nyc_high_schools, Strings.str_sat_scores)} 
                {this.renderRow(getIcon('ic_dash_favorites'), getIcon('ic_dash_news'), Strings.str_favorites, Strings.str_news)}
                {this.renderRow(getIcon('ic_dash_agency'), getIcon('ic_dash_search'), Strings.str_agency, Strings.str_search)}
            </View>
        )
    };

    /**
     * Method to render individual row.
     */
    renderRow = (aIcon1, aIcon2, aLabel1, aLabel2) => {
        return (
            <View style={appTheme.dash_board_row}>
                <TouchableOpacity style={appTheme.dash_board_item} onPress={() => { this.onDashActionClicked(aLabel1) }}>
                    <View>
                        <Image source={aIcon1} />
                        <Text style={appTheme.dash_board_item_txt}> {aLabel1}</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={appTheme.dash_board_item} onPress={() => { this.onDashActionClicked(aLabel2) }}>
                    <View>
                        <Image source={aIcon2} />
                        <Text style={appTheme.dash_board_item_txt}> {aLabel2}</Text>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
    onDashActionClicked = (actionStr) => {

        console.debug(" Result " + Strings.str_agency);
        switch (actionStr) {
            case Strings.str_nyc_high_schools: {
                moveToScreen(this, 'schoolList')
                break;
            } case Strings.str_sat_scores: {
                moveToScreen(this, 'scoreList')
                break;
            } case Strings.str_favorites: {
                moveToScreen(this, 'signup')
                break;
            } case Strings.str_news: {
                moveToScreen(this, 'login')
                break;
            } case Strings.str_search: {
                moveToScreen(this, 'login')
                break;
            } default: {

            }
        }

        console.info("Action -> " + actionStr);
        // this.setState({
        //     count: this.state.count + 1
        // });
    }
}
