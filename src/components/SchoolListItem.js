import React from "react";
import { Image, Text, TouchableOpacity, View } from 'react-native';
import common, { appTheme, padding, colors, app_test_border } from '../../res/styles/app_style';
import { moveToScreen } from "../utils/NavigationUtils";
import { getActionIcon, Strings } from '../utils/Resources';
import AppScreen from "./AppScreen";


export default class SchoolListItem extends AppScreen {

    onSchoolClicked = () => {
        this.props.onSchoolClicked(this.props.schoolInfo);
    }

    render() {

        return (
            <TouchableOpacity onPress={this.onSchoolClicked}>
                <View style={common.inner_container_with_border}>

                    <View style={common.row_container}>
                        <Text style={[appTheme.list_item_header_bold, { flex: 0.8 }]}>{
                            this.props.schoolInfo.school_name}
                        </Text>
                        <Text style={[appTheme.list_item_header, { flex: 0.2, textAlign: 'right' }]}>{this.props.schoolInfo.dbn}</Text>
                    </View>

                    <Text numberOfLines={4} ellipsizeMode={'tail'} style={[appTheme.list_item_content_text]}>{this.props.schoolInfo.overview_paragraph}</Text>

                    <View style={[common.row_container, { marginTop: padding.lg }]}>
                        <Image style={appTheme.action_icon} source={getActionIcon('ic_action_phone')} />
                        <Text style={appTheme.list_item_content_text}>{this.props.schoolInfo.phone_number}</Text>
                    </View>

                    <View style={common.row_container}>
                        <Image style={appTheme.action_icon} source={getActionIcon('ic_action_fax')} />
                        <Text style={appTheme.list_item_content_text}>{this.props.schoolInfo.fax_number}</Text>
                    </View>

                    <View style={common.row_container}>
                        <Image style={appTheme.action_icon} source={getActionIcon('ic_action_email')} />
                        <Text style={appTheme.list_item_content_text}>{this.props.schoolInfo.school_email}</Text>
                    </View>

                    <View style={common.row_container}>
                        <Image style={appTheme.action_icon} source={getActionIcon('ic_action_web')} />
                        <Text style={appTheme.list_item_content_text}>{this.props.schoolInfo.website}</Text>
                    </View>

                    <View style={common.row_container}>
                        <Image style={appTheme.action_icon} source={getActionIcon('ic_action_location')} />
                        <Text style={appTheme.list_item_content_text}>{this.props.schoolInfo.location}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }
}