import React, { Component } from 'react';
import { ActivityIndicator, FlatList, View } from 'react-native';
import { SearchBar } from 'react-native-elements';
import { moveToScreenWithExtra } from '../utils/NavigationUtils';
import { Strings } from '../utils/Resources';
import SchoolListItem from './SchoolListItem';

class SchoolListScreen extends Component {

    static navigationOptions = {
        title: Strings.str_NYC_High_School
    }

    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            data: [],
            page: 1,
            seed: 1,
            error: null,
            refreshing: false
        };
    }

    componentDidMount() {
        this.makeRemoteRequest();
    }

    makeRemoteRequest = () => {
        const { page, seed } = this.state;
        const url = 'https://data.cityofnewyork.us/resource/s3k6-pzi2.json'
        // AppConstant.APP_BASE_URI_STR + AppConstant.SCHOOL_INFO_URI;

        this.setState({ loading: true });

        fetch(url)
            .then(res => res.json())
            .then(res => {
                console.debug("Data Loading Success ! " + res);
                this.setState({
                    data: page === 1 ? res : [...this.state.data, ...res],
                    error: res.error || null,
                    loading: false,
                    refreshing: false
                });
            })
            .catch(error => {
                console.error("Data Loading Failed ! " + error);
                this.setState({ error, loading: false });
            });
    };

    handleRefresh = () => {
        this.setState(
            {
                page: 1,
                seed: this.state.seed + 1,
                refreshing: true
            },
            () => {
                this.makeRemoteRequest();
            }
        );
    };

    handleLoadMore = () => {
        this.setState(
            {
                page: this.state.page + 1
            },
            () => {
                this.makeRemoteRequest();
            }
        );
    };

    renderSeparator = () => {
        return (
            <View
                style={{
                    height: 1,
                    width: "86%",
                    backgroundColor: "#CED0CE",
                    marginLeft: "5%"
                }}
            />
        );
    };

    renderHeader = () => {
        return <SearchBar placeholder="Type Here..." lightTheme round />;
    };

    renderFooter = () => {
        if (!this.state.loading) return null;

        return (
            <View
                style={{
                    paddingVertical: 20,
                    borderTopWidth: 1,
                    borderColor: "#CED0CE"
                }}
            >
                <ActivityIndicator animating size="large" />
            </View>
        );
    };

    render() {
        return (
            <FlatList
                data={this.state.data}
                renderItem={this.renderListItem}
                extraData={this.state}
                keyExtractor={this.keyExtractor}
                ItemSeparatorComponent={this.renderSeparator}
                ListHeaderComponent={this.renderHeader}
                ListFooterComponent={this.renderFooter}
                onRefresh={this.handleRefresh}
                refreshing={this.state.refreshing}
                onEndReached={this.handleLoadMore}
                onEndReachedThreshold={50}
            />
        );
    }

    keyExtractor = (aSchoolInfo) => aSchoolInfo.dbn

    renderListItem = ({ item, index }) => {

        return (
            <SchoolListItem
                onSchoolClicked={this.onSchoolClicked}
                schoolInfo={item}
            />);
    }

    onSchoolClicked = (aSchoolInfo) => {
        console.info("Selected School " + aSchoolInfo.school_name);

        moveToScreenWithExtra(this, 'schoolDetail', {"Key_Extra_SchoolInfo": aSchoolInfo});
    }
}

export default SchoolListScreen;