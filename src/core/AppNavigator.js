import { createStackNavigator, createAppContainer, createDrawerNavigator, createBottomTabNavigator } from 'react-navigation';

import appStyle, { colors } from '../../res/styles/app_style';
import SplashScreen from '../components/SplashScreen'
import LoginScreen from '../components/LoginScreen'
import HomeScreen from '../components/HomeScreen'
import SignupScreen from '../components/SignupScreen'

import SATScoreListScreen from '../components/SATScoreListScreen'
import SchoolListScreen from '../components/SchoolListScreen'
import SchoolDetailScreen from '../components/SchoolDetailScreen'
const AppNavigator = createStackNavigator({
    splash: SplashScreen,
    home: HomeScreen,
    login: LoginScreen,
    signup: SignupScreen,
    scoreList: SATScoreListScreen,
    schoolList: SchoolListScreen,
    schoolDetail: SchoolDetailScreen
}, {
        defaultNavigationOptions: {
            headerStyle: {
                backgroundColor: colors.secondary,
                elevation: 0,
                shadowOpacity: 0
            },
            headerTintColor: '#333333',
            flex: 1,
            headerTitleStyle: {
                fontWeight: 'bold',
                color: '#ffffff'
            }
        }
    });

export default createAppContainer(AppNavigator);
