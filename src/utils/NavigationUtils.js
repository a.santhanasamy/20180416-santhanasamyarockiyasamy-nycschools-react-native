import React from 'react';
import { NavigationActions,StackActions } from 'react-navigation';

export const moveBack = (component) => {
  console.info("Moving back");
  component.props.navigation.pop();
}

export const moveToScreen = (component, aScreenName) => {
  console.info("Moving to screen " + aScreenName);
  component.props.navigation.navigate(aScreenName);
}

export const moveToScreenWithExtra = (component, aScreenName, aExtraObject) => {

  if(null == aExtraObject) {
    moveToScreen(component, aScreenName);
    return;
  }
  console.info("Moving to screen " + aScreenName);
  component.props.navigation.navigate(aScreenName, aExtraObject);
}

export const resetStackAction = (component, aScreen) => {
  const resetAction = StackActions.reset({
    index: 0, // <-- currect active route from actions array
    actions: [
      NavigationActions.navigate({ routeName: aScreen }),
    ],
  });
  component.props.navigation.dispatch(resetAction);
}

export const resetNavigation1 = (component, targetRoute) => {
  const resetAction = NavigationActions.reset({
    index: 0,
    actions: [
      NavigationActions.navigate({ routeName: targetRoute }),
    ],
  });
  component.props.navigation.dispatch(resetAction);
}