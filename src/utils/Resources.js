import React from 'react';


export const Strings = {
    str_agency: 'Agency',
    str_nyc_high_schools: 'NYC High Schools',
    str_favorites: 'Favorites',
    str_news: 'News',
    str_sat_scores: 'SAT Scores',
    str_search: 'Search',
    str_NYC_School: "NYC Schools",
    str_NYC_High_School: "NYC High Schools",
    str_NYC_school_detail: "NYC Schools Detail",
    str_SAT_score: "SAT Score"
}
export const IMAGES = {
    'ic_app_logo_white': require('../../res/icons/ic_app_logo_white.png'),
    'ic_dash_agency': require('../../res/icons/ic_dash_agency.png'),
    'ic_dash_school': require('../../res/icons/ic_dash_school.png'),
    'ic_dash_favorites': require('../../res/icons/ic_dash_favorites.png'),
    'ic_dash_news': require('../../res/icons/ic_dash_news.png'),
    'ic_dash_scores': require('../../res/icons/ic_dash_score.png'),
    'ic_dash_search': require('../../res/icons/ic_dash_search.png')
}
export const ACTION_IMAGES = {
    'ic_action_copy': require('../../res/icons/actions/ic_content_copy.png'),
    'ic_action_phone': require('../../res/icons/actions/ic_phone.png'),
    'ic_action_email': require('../../res/icons/actions/ic_email.png'),
    'ic_action_fax': require('../../res/icons/actions/ic_fax.png'),
    'ic_action_location': require('../../res/icons/actions/ic_location.png'),
    'ic_action_web': require('../../res/icons/actions/ic_web.png'),
    'ic_action_bus': require('../../res/icons/actions/ic_bus.png'),
    'ic_action_subway': require('../../res/icons/actions/ic_subway.png'),
    'ic_action_people': require('../../res/icons/actions/ic_people.png'),
    'ic_action_score': require('../../res/icons/actions/ic_sat_score.png'),
    'ic_action_math': require('../../res/icons/actions/ic_math.png'),
    'ic_action_reading': require('../../res/icons/actions/ic_reading.png'),
    'ic_action_writing': require('../../res/icons/actions/ic_writing.png'),
    'ic_action_play': require('../../res/icons/actions/ic_play.png'),
    'ic_action_search': require('../../res/icons/actions/ic_search.png'),
    'ic_action_timing': require('../../res/icons/actions/ic_timing.png'),
}

export const getIcon = (aResName) => {
    console.info(" Requested Icon " + IMAGES[aResName]);
    const thumbnail = IMAGES[aResName]
    return thumbnail;
}

export const getActionIcon = (aResName) => {
    console.info(" Requested Icon " + ACTION_IMAGES[aResName]);
    const thumbnail = ACTION_IMAGES[aResName]
    return thumbnail;
}
