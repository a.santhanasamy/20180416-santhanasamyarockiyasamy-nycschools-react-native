
export default {

    BASE_URL: 'http://someurl.com',

    // Base URI
    APP_BASE_URI_STR: 'https://data.cityofnewyork.us/',


    //  Path to the NYC High School Directory<br/>
    //  https://data.cityofnewyork.us/resource/s3k6-pzi2.json
    SCHOOL_INFO_URI: 'resource/s3k6-pzi2.json',

    SCHOOL_INFO_FILTER_URI: 'api/id/s3k6-pzi2.json',

    SCHOOL_COUNT_URI: 'api/id/s3k6-pzi2',

    //  Path to get the SAT Result for the school's in NYC <br/>
    //  https://data.cityofnewyork.us/resource/f9bf-2cp4.json
    SAT_SCORE_URI_STR: 'api/id/f9bf-2cp4.json',

    SAT_SCORE_COUNT_URI: 'api/id/f9bf-2cp4',

    SAT_SCORE_BY_ID_URI: 'api/id/f9bf-2cp4',

    // https://data.cityofnewyork.us/api/id/f9bf-2cp4?dbn :01M292

    NYCSchoolFilter: {
        SELECT: '$select',

        ORDER: '$order',

        LIMIT: '$limit',

        OFFSET: '$offset',

        SORT_ORDER: ':id ASC',

        SCHOOL_DBN_ID: 'dbn'

    },

    // NYC School list projection
    NYC_SCHOOL_QUERY_PARAM: 'dbn,school_name,school_email,school_sports,overview_paragraph,location,phone_number,fax_number,website,zip,start_time,end_time,bus,subway',

    //  NYC Schools Score info projection
    NYC_SCHOOL_SCORE_QUERY_PARAM: 'dbn,school_name,num_of_sat_test_takers,sat_critical_reading_avg_score,sat_math_avg_score,sat_writing_avg_score',
};