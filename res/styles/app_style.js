import { Dimensions, StyleSheet } from 'react-native';
import React from 'react';

export const dimensions = {
  match_h_parent: Dimensions.get('window').height,
  match_w_parent: Dimensions.get('window').width,
  action_bar_icon_size: 24
}

export const colors = {
  primary: '#64b5f6',
  secondary: '#134eb2',
  tertiary: '#5DA6A7',
  accent: '#ffffff',

  txt_primary: '#3d4040',
  txt_secondary: '#185074',
  txt_default: '#ffffff',

  app_border_color: '#d6d7da',
  dash_board_row_bg: '#ABE0C6',
  action_icon_color: '#052D84'
}

export const padding = {
  sm: 10,
  md: 20,
  lg: 30,
  xl: 40
}

export const app_test_border = {
  borderColor: colors.txt_secondary,
  borderRadius: 0,
  borderWidth: 1
}

export const fonts = {
  sm: 12,
  md: 18,
  lg: 24,
  xlg: 30,
  xxlg: 36
}

export const common = StyleSheet.create({
  root_container: {
    width: dimensions.match_w_parent,
    height: dimensions.match_h_parent,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.primary,
  },
  inner_container: {
    //...container,
    marginHorizontal: padding.md,
    marginVertical: padding.md,
  },

  inner_container_with_border: {
    marginHorizontal: padding.md,
    marginVertical: padding.md,
    padding: padding.md,
    borderColor: colors.txt_primary,
    borderRadius: 10,
    borderWidth: 5
  },
  
  row_container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },

  action_bar_icon: {
    width: dimensions.action_bar_icon_size,
    height: dimensions.action_bar_icon_size,
  }
});

export const appTheme = StyleSheet.create({
  splash_icon: {
    width: 300,
    height: 300,
  },

  action_icon: {
    width: 32,
    height: 32,
    tintColor: colors.action_icon_color,
    marginTop: padding.sm,
    marginBottom: padding.sm,
  },

  dash_board_icon: {
    width: 200,
    height: 200,
  },

  list_item_header: {
    color: colors.txt_secondary,
    fontSize: fonts.xxlg
  },

  list_item_header_bold: {
    color: colors.txt_secondary,
    fontSize: fonts.xxlg,
    fontWeight: 'bold',
  },

  list_item_content_header_text: {
    color: colors.txt_secondary,
    fontSize: fonts.xlg,
    marginTop: padding.lg
  },

  list_item_content_text: {
    color: colors.txt_primary,
    flex: 1,
    marginTop: padding.sm,
    marginBottom: padding.sm,
    marginHorizontal: padding.sm,
    fontSize: fonts.lg,
  },

  action_bar_center_title: {
    fontWeight: 'bold',
    color: colors.txt_default,
    alignSelf: 'center',
    textAlign: 'center',
    flex: 1
  },

  dash_board_container: {
    width: dimensions.match_w_parent,
    height: dimensions.match_h_parent,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.tertiary,
  },

  dash_board_row: {
    height: 300,
    flex: 1,
    flexDirection: 'row',
    backgroundColor: colors.dash_board_row_bg,
  },

  dash_board_item: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.txt_primary,
    width: 350,
    padding: 25,
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: colors.app_border_color,
  },

  dash_board_item_txt: {
    alignItems: 'center',
    //backgroundColor: colors.txt_secondary,
    color: colors.txt_default,
    marginTop: padding.sm,
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: fonts.lg,
  },

});

export default common;